"use strict";
const _ = require("lodash");
const kleur = require("kleur");

const {SlashCommandBuilder} = require("@discordjs/builders");
const {REST} = require("@discordjs/rest");
const {Routes} = require("discord-api-types/v9");
const {Client} = require("discord.js");
const {MoleculerClientError} = require("moleculer").Errors;

let capitalize = (s) => s[0].toUpperCase() + s.slice(1);

module.exports = {
    mixins: [],
    settings: {
        token: "",
        clientOpts: {},
    },
    messages: {},
    slashes: {},
    actions: {
        interaction: {
            visibility: "private",
            params: {
                interaction: "object",
                opts: {type: "object", default: {}}
            },
            async handler(ctx) {
                let {interaction, opts} = ctx.params;
                ctx.meta = opts.meta || ctx.meta;
                ctx.params = opts.params || {};

                for (let optionName in opts.options || {}) {
                    let option = opts.options[optionName];
                    let discordOptionName = _.snakeCase(optionName);
                    ctx.params[optionName] = interaction.options['get' + capitalize(option.type)](discordOptionName);
                }
                let result;
                try {
                    await this.authorize(ctx, interaction);
                    await this.onBeforeCall(ctx, interaction);

                    if (opts.deferred) {
                        await interaction.deferReply(typeof opts.deferred === "object" ? opts.deferred : undefined);
                    }
                    let msg = `Calling `;
                    msg += kleur.yellow(typeof opts.action === "string" ? opts.action : "local function");
                    msg += " to handle ";
                    if (interaction.isCommand()) {
                        msg += kleur.cyan('/' + interaction.commandName);
                    } else if (interaction.isButton()) {
                        msg += kleur.cyan('/' + interaction.commandName);
                    }
                    this.logger.info(msg);
                    if (typeof opts.action === "string") {
                        result = await ctx.call(opts.action, ctx.params, ctx);
                    } else {
                        result = await opts.action.call(this, ctx, interaction);
                    }

                } catch (e) {
                    e = this.onError(ctx, interaction, e) || e;
                    this.logger.info(e);
                    result = e.toString ? e.toString() : e;
                }

                //registering collector for each input component
                for (let actionRow of result?.components || []) {
                    const customIds = actionRow.components.map(component => component.custom_id);
                    const selectComponents = actionRow.components.filter(component => component.type === 3);
                    for (let selectComponent of selectComponents) {
                        for (let [i, option] of selectComponent.options.entries()) {
                            if (option.params) {
                                option.value = i.toString();
                            } else {
                                option.params = option.value;
                                if (typeof option.value === 'object') option.value = JSON.stringify(option.value);
                            }
                        }
                    }

                    let {time, max, maxUsers, idle, dispose, onTimeout} = actionRow;
                    if (result.delete) time = result.delete;

                    // Filtering button by userId and customId
                    const filter = (buttonInteraction) => {
                        let userId;
                        if (actionRow.userId) userId = Array.isArray(actionRow.userId) ? actionRow.userId : [actionRow.userId]
                        return customIds.includes(buttonInteraction.customId) && (userId ? userId.includes(buttonInteraction.user.id) : true)
                    }
                    const collect = (interaction) => {
                        let component = actionRow.components.find(component => component.custom_id === interaction.customId);
                        if (interaction.isButton()) {
                            let opts = {...actionRow, action: component.action};
                            this.buttonHandler(interaction, opts);
                        } else if (interaction.isSelectMenu()) {
                            let maxValues = component.max_values || 1;
                            let option;
                            if (maxValues === 1) {
                                option = component.options.find(option => option.value === interaction.values[0]).params;
                            } else {
                                option = component.options
                                    .filter(option => interaction.values.includes(option.value))
                                    .map(option => option.params);
                            }
                            let params = {...(component.params || {}), [component.custom_id]: option}
                            let opts = {params, action: component.action};
                            this.selectHandler(interaction, opts);
                        }
                    }
                    const end = (collected) => {
                        if (onTimeout) this.broker.call(onTimeout, opts.params || {}, opts);
                    }

                    const collector = interaction.channel.createMessageComponentCollector(
                        {filter, time, max, maxUsers, idle, dispose});
                    collector.on('collect', collect);
                    collector.on('end', end);
                }

                if (result) {
                    if (Array.isArray(result)) {
                        for (let item of result) {
                            await this.reply(interaction, item);
                        }
                    } else {
                        await this.reply(interaction, result);
                    }
                }
            }
        },
        guilds: {
            params: {
                guildId: [{type: "number", integer: true, optional: true}, {type: "string", optional: true}]
            },
            handler(ctx) {
                if (ctx.params.guildId) {
                    let guild = this.client.guilds.cache.find(guild => guild.id === ctx.params.guildId);
                    if (!guild) throw new MoleculerClientError("GUILD_NOT_FOUND", 404, "GUILD_NOT_FOUND");
                    return JSON.parse(JSON.stringify(guild));
                }
                return JSON.parse(JSON.stringify(this.client.guilds.cache));
            }
        },
        channels: {
            params: {
                channelId: [{type: "number", integer: true, optional: true}, {type: "string", optional: true}]
            },
            async handler(ctx) {
                if (ctx.params.channelId) {
                    let channel = await this.client.channels.cache.get(ctx.params.channelId);
                    if (!channel) throw new MoleculerClientError("GUILD_NOT_FOUND", 404, "GUILD_NOT_FOUND");
                    return JSON.parse(JSON.stringify(channel));
                }
                return JSON.parse(JSON.stringify(this.client.channels.cache));
            }
        },
        users: {
            params: {
                userId: [{type: "string"}, {type: "array", items: "string"}],
            },
            async handler(ctx) {
                let result;
                let userId = ctx.params.userId;
                if (Array.isArray(userId)) {
                    let promises = userId.map((item) => this.client.users.fetch(item));
                    result = await Promise.all(promises);
                } else {
                    result = await this.client.users.fetch(userId);
                    if (!result) throw new MoleculerClientError("USER_NOT_FOUND", 404, "USER_NOT_FOUND");
                }
                return JSON.parse(JSON.stringify(result));
            }
        },
        send: {
            params: {
                channelId: {type: "string", empty: false, optional: true},
                userId: {type: "string", empty: false, optional: true},
                message: [{type: "string"}, {type: "object",}]
            },
            async handler(ctx) {
                let {channelId, userId, message} = ctx.params;
                message = this.formatMessage(message);
                if (!channelId && !userId) {
                    throw new MoleculerClientError("NO_RECIPIENT", 422, "NO_RECIPIENT", {msg: "You need to set a recipient"});
                }
                if (userId) {
                    let user = await this.client.users.fetch(userId, false);
                    user.send(message);
                }
                if (channelId) {
                    let channel = await this.client.channels.cache.get(channelId);
                    channel.send(message);
                }
            }
        },
        clear: {
            params: {
                channelId: {type: "string", convert: true},
                messageId: {type: "string", convert: true, optional: true},
                amount: {type: "number", integer: true, positive: true, optional: true},
            },
            async handler(ctx) {
                let {channelId, messageId, commandId, amount} = ctx.params;
                if (!channelId && !messageId && !commandId) {
                    throw new MoleculerClientError("NO_MESSAGES_TO_DELETE", 422, "NO_MESSAGES_TO_DELETE", {msg: "You need to set a messageId or amount"});
                }
                let channel = await this.client.channels.cache.get(channelId);
                if (amount) {
                    if (!channel) throw new MoleculerClientError("GUILD_NOT_FOUND", 404, "GUILD_NOT_FOUND");
                    for (let i = amount; i > 0; i -= 100) {
                        if (i >= 100) await channel.bulkDelete(100);
                        else if (i < 100 && i > 0) await channel.bulkDelete(i);
                    }
                } else if (messageId) {
                    channel.messages.fetch(messageId).then(message => message.delete());
                }
            }
        },
    },
    methods: {
        authorize(ctx, interaction) {
        },
        onBeforeCall(ctx, interaction) {
        },
        onAfterCall(ctx, interaction, response) {
        },
        onError(ctx, interaction, error) {
        },
        formatMessage(content) {
            if (typeof content === "object" && content.type === "embed") {
                content = {embeds: [content], ephemeral: content.ephemeral};
            } else if (typeof content === "object" && content.embed) {
                content = {embeds: [content.embed]};
            }
            return content;
        },
        reply(interaction, content) {
            content = this.formatMessage(content);

            if (content.delete) {
                if (typeof content.delete === "number") {
                    setTimeout(() => interaction.deleteReply(), content.delete)
                } else if (typeof content.delete === "object") {
                    setTimeout(() => {
                        interaction.ephemeral ? interaction.editReply(content.delete): interaction.deleteReply();
                    }, content.delete.timeout || 10);
                } else {
                    return interaction.delete();
                }
            }
            if (content.type === "update") {
                return interaction.update(content);
            } else if (interaction.replied) {
                return interaction.followUp(content);
            } else if (interaction.deferred) {
                return interaction.editReply(content);
            } else {
                return interaction.reply(content)
            }
        },
        startedDiscordHandler() {
            this.logger.info("Discord Gateway Started");
        },
        async messageHandler(message) {
            let messageEvents = this.schema.messages;
            for (let eventName in messageEvents) {
                let messageContent = message.content;
                if (messageContent.match(messageEvents[eventName].regex)) {
                    let regexContent = messageContent;
                    let params = {};
                    for (let optionName in messageEvents[eventName].options) {
                        let option = messageEvents[eventName].options[optionName];
                        let regexResult = new RegExp(option.regex,).exec(regexContent);
                        if (regexResult) {
                            regexContent = regexContent.substring(regexResult.index + regexResult[0].length);
                            params[optionName] = regexResult[0];
                        }
                    }
                    let ctx = {meta: {}};
                    try {
                        await this.authorize(ctx, message);
                        await this.onBeforeCall(ctx, message);
                        let response = await this.broker.call(messageEvents[eventName].action, params, ctx);
                        await this.onAfterCall(ctx, message, response);
                    } catch (e) {
                        this.onError(ctx, message, e);
                    }
                }
            }
        },
        interactionHandler(interaction, opts = {}) {
            if (interaction.isCommand()) {
                return this.slashCommandHandler(interaction, opts);
            } else if (interaction.isButton()) {
                // return this.buttonHandler(interaction, opts);
            }
        },
        async selectHandler(interaction, opts) {
            this.logger.info(`Select Command ${kleur.cyan(interaction.customId)} received`)
            return this.callAction(interaction, opts);
        },
        async buttonHandler(interaction, opts) {
            this.logger.info(`Button Command ${kleur.cyan(interaction.customId)} received`)
            return this.callAction(interaction, opts);
        },
        async slashCommandHandler(interaction) {
            let {commandName} = interaction;
            //finding commandName in slashes
            commandName = Object.keys(this.schema.slashes).find(slash => _.camelCase(commandName) === _.camelCase(slash));
            let slash = this.schema.slashes[commandName];
            this.logger.info(`Slash Command ${kleur.cyan('/' + commandName)} received`);
            if (slash) {
                return this.callAction(interaction, slash);
            }
        },
        async callAction(interaction, opts = {}) {
            return this.actions.interaction({interaction, opts});
        }
    },
    created() {
        const client = new Client(this.settings.clientOpts);
        this.client = client;
        this.client.once("ready", this.startedDiscordHandler);
        this.client.on("messageCreate", this.messageHandler);
        this.client.on("interactionCreate", this.interactionHandler);

        process.on("unhandledRejection", error => {
            this.logger.error("Test error:", error);
        });

        // Setting Message Commands
        let messageEvents = this.schema.messages;
        for (let eventName in messageEvents) {
            let messageEvent = messageEvents[eventName];
            let params = eventName.match(/(?<=\{).*?(?=\})/igm) || [];
            messageEvent.options = {};
            params = params.map(param => {
                let wildcard = `{${param}}`;
                let matchIndex = eventName.indexOf(wildcard);
                let endIndex = matchIndex + wildcard.length;
                return {name: param, wildcard, matchIndex, endIndex}
            });
            params.forEach(param => messageEvent.options[param.name] = {})
            let regex = eventName;
            for (let [i, param] of params.entries()) {
                let endOfLastParam = params[i - 1]?.endIndex || 0;
                let startOfNextParam = params[i + 1]?.matchIndex || eventName.length;
                let start = eventName.substr(endOfLastParam, param.matchIndex - endOfLastParam);
                let end = eventName.substr(param.endIndex, startOfNextParam - param.endIndex);
                let optionRegex = "";
                if (!start) {
                    optionRegex += "^";
                } else {
                    optionRegex += "(?<=" + start + ")";
                }
                optionRegex += ".*?";
                if (!end) {
                    optionRegex += "$";
                } else {
                    optionRegex += "(?=" + end + ")";
                }
                messageEvent.options[param.name].regex = optionRegex;
                regex = regex.replace(param.wildcard, "(.*)");
            }
            messageEvent.regex = new RegExp("^" + regex, "gm");
            if (typeof messageEvent.action === "string") {
                this.logger.info(`Message Command ${kleur.cyan(eventName) + kleur.gray(" => ") + kleur.green(messageEvent.action)} registered`)
            } else {
                `Message Command ${kleur.cyan(eventName) + kleur.gray(" => ") + kleur.green(" local function ")} registered`
            }
        }

        let commands = [];
        for (let slashName in this.schema.slashes) {
            let slashSettings = this.schema.slashes[slashName];
            slashSettings.options = slashSettings.options || {};
            let slashCommand = new SlashCommandBuilder();

            // Setting Slash Command Config
            slashCommand.setName(_.snakeCase(slashName));
            slashCommand.setDescription(slashSettings.description);

            // Setting Slash Command Input Options
            for (let optionName in slashSettings.options) {
                let optionSetting = slashSettings.options[optionName];
                optionName = _.snakeCase(optionName);
                let methodName = 'add' + capitalize(optionSetting.type) + 'Option';
                slashCommand[methodName](option => {
                    let {type, ...optConfigs} = optionSetting;
                    //Setting Slash Command Option Settings
                    option.setName(optionName);
                    for (let optConfig in optConfigs) {
                        option['set' + capitalize(optConfig)](optConfigs[optConfig])
                    }
                    return option;
                })
            }
            commands.push(slashCommand);
            if (typeof slashSettings.action === "string") {
                this.logger.info(`Slash Command ${kleur.cyan('/' + slashName) + kleur.gray(' => ') + kleur.green(slashSettings.action)} registered`);
            } else {
                this.logger.info(`Slash Command ${kleur.cyan('/' + slashName) + kleur.gray(' => ') + kleur.green('function')} registered`);
            }

        }
        commands = commands.map(command => command.toJSON());
        const rest = new REST({version: "9"}).setToken(this.settings.token);
        if (!this.settings.guildId) {
            rest
                .put(Routes.applicationCommands(this.settings.clientId), {body: commands})
                .then(() => this.logger.info("Successfully registered application commands."))
                .catch(console.error);
        } else {
            let guildIds = Array.isArray(this.settings.guildId) ? this.settings.guildId : [this.settings.guildId];
            for (let guildId of guildIds) {
                rest
                    .put(Routes.applicationGuildCommands(this.settings.clientId, guildId), {body: commands})
                    .then(() => this.logger.info("Successfully registered application commands."))
                    .catch(console.error);
            }
        }
    },
    started() {
        this.client.login(this.settings.token);
    },
    stopped() {
        this.client.destroy()
    }
};
