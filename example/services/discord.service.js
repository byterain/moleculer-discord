"use strict";

const DiscordGateway = require("../../index");

module.exports = {
    name: "discord",
    mixins: [DiscordGateway],
    settings: {
        clientOpts: {intents: ["GUILDS", "GUILD_MESSAGES", "GUILD_MESSAGE_REACTIONS"]},
        token: "XXXXX",
        guildId: "XXXXX",
        clientId: "XXXXXX",
    },
    messages: {
        "!ping": {action: "users.ping"},
        "!say {message}": {action: "users.say"},
    },
    slashes: {
        ping: {
            action: "users.ping",
            description: "Replies with pong!",
        },
        server: {
            async action(ctx, interaction) {
                return await interaction.reply("Server info.");
            },
            description: "Replies with server info!",
        },
        user: {
            action: "users.get",
            description: "Replies with user info!",
            options: {
                user: {type: "user", description: "User to get Information", required: true},
            },
        },
        perfil: {
            action: "users.perfil",
            description: "Replies with user info!",
            options: {
                user: {type: "user", description: "User to get Information", required: true},
            },
        },
        clear: {
            action: "users.clear",
            description: "Clean Messages",
            options: {
                amount: {type: "integer", description: "How many messages to deletee", required: true,},
                channel: {type: "channel", description: "Channel that will delete messages"}
            },
        },
        guilds: {
            action: "users.guilds",
            description: "Show guilds",
            options: {},
        },
        channels: {
            action: "users.channels",
            description: "Show channels",
            options: {},
        },
        say: {
            action: "users.say",
            description: "Replies with user info!",
            options: {
                channel: {type: "channel", description: "Which channel to send messages", required: true,},
                message: {
                    type: "string", description: "What messages to send", required: true,
                }
            }
        },
        longTask: {
            action: "users.longTask",
            description: "Long Task",
            deferred: {ephemeral: true},
        },
        buttons: {
            action: "users.buttons",
            description: "Show Buttons",
        },
        select: {
            action: "users.select",
            description: "Show Select component",
        }
    },
    methods: {
        onBeforeCall(ctx, interaction) {
            ctx.meta.author = interaction.user;
            ctx.meta.channel = interaction.channel;
        }
    }
};
