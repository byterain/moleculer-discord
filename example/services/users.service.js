"use strict";

module.exports = {
    name: "users",
    settings: {},
    actions: {
        get: {
            handler(ctx) {
                let user = ctx.params.user;
                return `This user is ${user.username} with id ${user.id} and he is ${user.bot ? '' : 'not'} a bot`;
            }
        },
        ping: {
            async handler(ctx) {
                return {content: "pong", delete: {timeout: 5000, content: 'Timeout your ping!!'}, ephemeral: true}
            }
        },
        guilds: {
            async handler(ctx) {
                let guilds = await ctx.call("discord.guilds");
                return "Guilds " + guilds.map(guild => guild.name).join(', ');
            }
        },
        channels: {
            async handler(ctx) {
                let channels = await ctx.call("discord.channels");
                return "Channels " + channels.map(guild => guild.name).join(', ');
            }
        },
        clear: {
            params: {
                channel: {type: "object", optional: true},
                amount: {type: "number", positive: true, integer: true}
            },
            async handler(ctx) {
                let {channel, amount} = ctx.params;
                if (!channel) channel = ctx.meta.channel;
                await ctx.call("discord.clear", {channelId: channel.id, amount: amount});
                return "Messages Deleted";
            }
        },
        perfil: {
            handler(ctx) {
                let author = ctx.meta.author;
                return {
                    type: "embed",
                    title: "AA VANILLA CLUB",
                    color: "#C71585",
                    url: "https://discord.js.org",
                    description: `${author.name} seja bem vindo!`,
                    author: {
                        name: author.username,
                        icon_url: `https://cdn.discordapp.com/avatars/${author.id}/${author.avatar}.gif?size=100`,
                        url: "https://discord.js.org"
                    },
                    thumbnail: {url: `https://cdn.discordapp.com/avatars/${author.id}/${author.avatar}.gif?size=100`},
                    fields: [
                        {name: "Título de uma descrição", value: "Uma descrição"},
                        {name: "Sua conta foi criada em :", value: "Data"},
                        {name: "Título de uma descrição", value: "Uma descrição", inline: true},
                        {name: "Título de uma descrição", value: "Uma descrição", inline: true},
                    ],
                    timestamp: new Date(),
                    image: "https://cdn.discordapp.com/attachments/901826669348794368/904491938756051034/translogo.png",
                    footer: {
                        text: "Some footer text here",
                        icon_url: "https://cdn.discordapp.com/attachments/901826669348794368/904491938756051034/translogo.png"
                    }
                }
            }
        },
        say: {
            params: {channel: {type: "object", optional: true}, message: "string"},
            async handler(ctx) {
                let {channel, message} = ctx.params;
                if (channel) {
                    await ctx.call("discord.send", {
                        channelId: channel.id,
                        userId: ctx.meta.author.id,
                        message: message
                    })
                } else {
                    await ctx.call("discord.send", {channelId: ctx.meta.channel.id, message: message})
                }
                return "Messages Sent";
            }
        },
        longTask: {
            async handler() {
                await (new Promise((resolve, reject) => {
                    setTimeout(resolve, 10000)
                }));
                return "Long Task is done";
            }
        },
        buttons: {
            async handler() {
                return {
                    content: "*`Message for the Buttons`*",
                    delete: 10000,
                    components: [
                        {
                            type: 1,
                            params: {foo: "bar"},
                            time: 10000,
                            components: [{
                                type: 2,
                                label: "Verify",
                                style: 3,
                                custom_id: "verify",
                                action: "users.acceptButton",
                            }, {
                                type: 2,
                                label: "Cancel",
                                style: 4,
                                custom_id: "cancel",
                                action: "users.cancelButton",
                            }
                            ]
                        }
                    ]
                };
            }
        },
        acceptButton: {
            handler(ctx) {
                return {type: "update", content: "OK", components: []}
            }
        },
        cancelButton: {
            handler(ctx) {
                return {type: "update", content: "Canceled", components: []}
            }
        },
        select: {
            handler(ctx) {
                return {
                    content: "Select Language",
                    ephemeral: true,
                    components: [
                        {
                            type: 1,
                            components: [
                                {
                                    type: 3,
                                    placeholder: "Choose a language",
                                    custom_id: "language",
                                    action: "users.selectLanguage",
                                    min_values: 1,
                                    max_values: 1,
                                    options: [{
                                        label: "English",
                                        params: "en_us",
                                        emoji: {
                                            id: null,
                                            name: "🇺🇸"
                                        }
                                    }, {
                                        label: "Português",
                                        params: "pt_br",
                                        emoji: {
                                            id: null,
                                            name: "🇧🇷"
                                        }
                                    }, {
                                        label: "日本語",
                                        params: "jp_ja",
                                        emoji: {
                                            id: null,
                                            name: "🇯🇵"
                                        }
                                    }],
                                },
                            ],
                        }
                    ]
                };
            }
        },
        selectLanguage: {
            params: {
                language: "string",
            },
            handler(ctx) {
                return {type: "update", content: `OK, Language setted to ${ctx.params.language}!!`, components: []}
            }
        },
    }
};
